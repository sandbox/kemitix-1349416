<?php

/**
 * Form builder. Configure frapi_client.
 *
 * @ingroup forms
 * @see system_settings_form()
 */
function frapi_client_settings() {
  return system_settings_form(frapi_client_settings_form());
}

function frapi_client_settings_form() {
  $form['frapi_client_server'] = array(
    '#type' => 'textfield',
    '#title' => t('Web Services API server'),
    '#default_value' => variable_get('frapi_client_server', 'http://localhost'),
    '#description' => t('URL for api server'),
  );
  $form['frapi_client_user'] = array(
    '#type' => 'textfield',
    '#title' => t('Web Services user'),
    '#default_value' => variable_get('frapi_client_user', 'paste your user id here'),
    '#description' => t('Username for api server'),
  );
  $form['frapi_client_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Web Services Key'),
    '#default_value' => variable_get('frapi_client_key', 'paste your API key here'),
    '#description' => t('Key for api server'),
  );
  return $form;
}
