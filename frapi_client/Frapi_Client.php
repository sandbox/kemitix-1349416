<?php

class Frapi_Client {

  private $server;
  private $id;
  private $key;

  private $cache_enable;
  private $error;
  
  public function __construct($server, $id, $key) {
    $this->server = $server;
    $this->id = $id;
    $this->key = $key;
    $this->caching(TRUE);
    $this->error = NULL;
  }

  public function caching($flag = NULL) {
    if (isset($flag)) {
      $this->cache_enable = (boolean)$flag;
    }
    return $this->cache_enable;
  }
  
  private function action_url($action) {
    return $this->server . $action .'.php';
  }

  /**
   * Returns the data structure of the method called.
   */
  public function call($method, $action, $parameters) {
    switch ($method) {
      case 'GET':
        $response = $this->get($action, $parameters);
        break;
      case 'POST':
        $response = $this->post($action, $parameters);
        break;
      default:
        throw new Exception("Unsupported method: $method");
    }
    if ($this->error) {
      return $this->error;
    }
    $data = unserialize($response);
    return $data;
  }

  private function get($action, $parameters = array()) {
    $url = $this->action_url($action);
    # append parameters to query string
    $url .= $this->build_parameters($parameters);
    $curl = $this->create_curl(CURLOPT_HTTPGET, $url);
    $results = curl_exec($curl);
    $this->catch_curl_errors($curl);
    $this->curl_close($curl);
    return $results;
  }

  private function post($action, $parameters) {
    $url = $this->action_url($action);
    $curl = $this->create_curl(CURLOPT_POST, $url);
    $fields = $this->build_parameters($parameters, '&');
    # insert parameters into POST request
    curl_setopt($curl, CURLOPT_POSTFIELDS, $fields);
    $results = curl_exec($curl);
    $this->catch_curl_errors($curl);
    $this->curl_close($curl);
    return $results;
  }
  
  /**
   * Only close curl resources when they aren't being cached.
   * Otherwise what is in the cache is unusable.
   *
   * @param type $curl 
   */
  private function curl_close($curl) {
    if (!$this->caching()) {
      curl_close($curl);
    }
  }

  private function catch_curl_errors($curl) {
    if (curl_errno($curl)) {
      $this->error = curl_error($curl);
    }
  }

  private function build_parameters($parameters, $prefix = '?') {
    $output = '';
    if (count($parameters) > 0) {
      $params = array();
      foreach ($parameters as $key => $value) {
        $value = preg_replace('/\=/', '%3D', $value);
        $value = preg_replace('/,/', '%2C', $value);
        $params[] = $key .'='. $value;
      }
      $output .= $prefix . join('&', $params);
    }
    return $output;
  }

  private function create_curl($method, $url) {
    static $cache = array();
    if ($this->caching()) {
      $cachekey = $method . $url;
      if (array_key_exists($cachekey, $cache)) {
        return $cache[$cachekey];
      }
    }
    $curl = curl_init($url);
    curl_setopt($curl, $method, TRUE);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($curl, CURLOPT_HTTPPROXYTUNNEL, FALSE);
    curl_setopt($curl, CURLINFO_HEADER_OUT, TRUE);
    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_DIGEST);
    $userpwd = $this->id .':'. $this->key;
    curl_setopt($curl, CURLOPT_USERPWD, $userpwd);
    
    $cache[$cachekey] = $curl;
    return $curl;
  }

}

